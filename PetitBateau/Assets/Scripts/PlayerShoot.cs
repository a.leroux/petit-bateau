﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public float bulletForce;
    public Transform shotPoint;
    public GameObject prefabBullet;
    public float coolDown;
    private bool coolDownShot = true;

    void Update()
    {
        if (coolDownShot == true)
        {
            PlayerController();
        }
    }

    void PlayerController()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            GameObject bullet = Instantiate(prefabBullet, shotPoint.position, shotPoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(shotPoint.up * bulletForce, ForceMode2D.Impulse);

            StartCoroutine(Teste());

            IEnumerator Teste()
            {
                coolDownShot = false;
                yield return new WaitForSeconds(coolDown);
                coolDownShot = true;
            }
        }
    }
}
